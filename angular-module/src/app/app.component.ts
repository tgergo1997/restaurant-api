import { Component } from '@angular/core';
import {Router} from '@angular/router';
import {AuthenticationService} from "./services/authentication.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  email!: string|null;

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router
  ) {
  }

  public isUserLoggedIn() {
    this.email = this.authenticationService.getLoggedInUserEmail();
    return this.email != null;
  }

  public logOut() {
    this.authenticationService.logOut();
  }

  public gotoRegiser() {
    this.router.navigate(['register']);
  }

  public gotoLogin() {
    this.router.navigate(['login']);
  }

  public gotoRestaurants() {
    this.router.navigate(['my-restaurants']);
  }
}
