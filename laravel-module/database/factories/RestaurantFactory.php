<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class RestaurantFactory extends Factory
{
    public function definition(): array
    {
        return [
            'user_id' => User::pluck('id')->random(),
            'name' => $this->faker->word,
            'description' => $this->faker->text,
            'opening_time' => '0' . $this->faker->numberBetween(6, 9) . ':00',
            'closing_time' => $this->faker->numberBetween(16, 23) . ':00',
        ];
    }
}
