import { Component, OnInit } from '@angular/core';
import {Restaurant} from "../../../models/Restaurant";
import {RestaurantService} from "../../../services/restaurant.service";
import { MatDialog } from "@angular/material/dialog"
import {FormComponent} from "../form/form.component";

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

  restaurant!: Restaurant;
  myRestaurants!: Restaurant[];
  errors!: string[];

  constructor(
    private restaurantService: RestaurantService,
    private dialog: MatDialog
  ) {
    this.restaurant = new Restaurant();
    this.myRestaurants = [];
    this.errors = [];
  }

  public ngOnInit() {
    this.fetchMyRestaurants();
  }

  public openDialog(restaurant?: Restaurant): void {
    if (this.dialog.openDialogs.length == 1) {
      return;
    }

    this.restaurant = restaurant ? restaurant : new Restaurant();

    const dialogRef = this.dialog.open(FormComponent, {
      data: { ...this.restaurant },
    });

    dialogRef.componentInstance.onSave.subscribe((restaurant: Restaurant) => {
      if (restaurant.id) {
        this.restaurantService.update(restaurant).subscribe(() => this.fetchMyRestaurants());
      } else {
        this.restaurantService.create(restaurant).subscribe(() => this.fetchMyRestaurants());
      }
      dialogRef.close();
    });
  }

  public deleteRestaurant(restaurant: Restaurant): void {
    if (!confirm('Biztosan törölni szeretné a ' + restaurant.name + ' éttermet?')) {
      return;
    }
    this.restaurantService.delete(restaurant).subscribe(() => this.fetchMyRestaurants());
  }

  private fetchMyRestaurants() {
    this.restaurantService.listMyRestaurants().subscribe((response: any) => {
      this.myRestaurants = response.data;
    });
  }
}
