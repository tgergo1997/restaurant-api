import { Component, Inject, EventEmitter } from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {Restaurant} from "../../../models/Restaurant";

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent {

  onSave = new EventEmitter();

  constructor(
    private dialogRef: MatDialogRef<FormComponent>,
    @Inject(MAT_DIALOG_DATA) public restaurant: Restaurant,
  ) {}

  public save() {
    this.onSave.emit(this.restaurant);
  }
}
