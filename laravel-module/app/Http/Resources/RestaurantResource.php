<?php

namespace App\Http\Resources;

use App\Models\Restaurant;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin Restaurant */
class RestaurantResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'opening_time' => Carbon::parse($this->opening_time)->format('H:i'),
            'closing_time' => Carbon::parse($this->closing_time)->format('H:i'),
        ];
    }
}
