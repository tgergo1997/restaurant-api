import { Component } from '@angular/core';
import {User} from "../../models/User";
import {AuthenticationService} from "../../services/authentication.service";
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {

  user!: User;
  errors!: string[];

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router,
  ) {
    this.user = new User();
    this.errors = [];
  }

  public register() {
    this.authenticationService.register(this.user).subscribe(
      () => this.router.navigate(['/login']),
      (response) => this.errors = Object.values(response.error.errors)
    );
  }
}
