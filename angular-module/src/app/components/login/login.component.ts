import { Component } from '@angular/core';
import {AuthenticationService} from "../../services/authentication.service";
import {User} from "../../models/User";
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  user!: User;
  error!: any;

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router
  ) {
    this.user = new User();
  }

  public login() {
    this.authenticationService.login(this.user).subscribe(
      (response: any) => {
        sessionStorage.setItem('token', response.success.token);
        sessionStorage.setItem('email', this.user.email);
        this.router.navigate(['/']);
      },
      () => this.error = true
    );
  }
}
