<?php

namespace App\Http\Controllers;

use App\Actions\UserTokenAction;
use App\Http\Requests\RegisterRequest;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function login(Request $request, UserTokenAction $action): JsonResponse
    {
        if (!$this->attemptLogin($request)) {
            return response()->json(['error' => 'Unauthorised'], 401);
        }

        /** @var User $user */
        $user = auth()->user();

        return response()->json(['success' => [
            'token' => $action->createUserToken($user)
        ]]);
    }

    private function attemptLogin(Request $request): bool
    {
        return auth()->attempt(['email' => $request->get('email'), 'password' => $request->get('password')]);
    }

    public function register(RegisterRequest $request, UserTokenAction $action): JsonResponse
    {
        $data = $request->all();
        $data['password'] = bcrypt($data['password']);
        $user = User::create($data);

        return response()->json(['success' => [
            'token' => $action->createUserToken($user)
        ]]);
    }
}
