export class Restaurant {
  id!: number;
  name!: string;
  description!: string;
  opening_time!: Date;
  closing_time!: Date;
}
