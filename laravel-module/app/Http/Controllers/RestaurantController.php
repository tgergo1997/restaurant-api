<?php

namespace App\Http\Controllers;

use App\Actions\RestaurantFilterAction;
use App\Http\Requests\RestaurantRequest;
use App\Http\Resources\RestaurantResource;
use App\Models\Restaurant;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;

class RestaurantController extends Controller
{
    public function index(): AnonymousResourceCollection
    {
        return RestaurantResource::collection(Restaurant::all());
    }

    public function myRestaurants(RestaurantFilterAction $action): AnonymousResourceCollection
    {
        return RestaurantResource::collection($action->filerRestaurants(['user_id' => auth()->id()]));
    }

    public function store(RestaurantRequest $request): RestaurantResource
    {
        $data = $request->all();
        $data['user_id'] = auth()->id();
        return new RestaurantResource(Restaurant::create($data));
    }

    public function show(Restaurant $restaurant): RestaurantResource
    {
        return new RestaurantResource($restaurant);
    }

    public function update(Restaurant $restaurant, RestaurantRequest $request): RestaurantResource
    {
        $restaurant->update($request->all());
        return new RestaurantResource($restaurant);
    }

    public function destroy(Restaurant $restaurant): Response
    {
        $restaurant->delete();
        return response()->noContent();
    }
}
