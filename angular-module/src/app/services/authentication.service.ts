import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import{ GlobalConstants } from '../common/global-constants';
import {User} from "../models/User";

@Injectable()
export class AuthenticationService {

  private readonly url: string;

  constructor(private http: HttpClient) {
    this.url = GlobalConstants.apiURL;
  }

  public login(user: User) {
    return this.http.post(this.url + 'login', { email: user.email, password: user.password });
  }

  public register(user: User) {
    return this.http.post(this.url + 'register', user);
  }

  public getLoggedInUserEmail() {
    return sessionStorage.getItem('email');
  }

  public logOut() {
    sessionStorage.clear();
  }
}
