<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    /**
     * @param $request
     * @param array $guards
     * @return void
     */
    protected function unauthenticated($request, array $guards): void
    {
        abort(401);
    }
}
