<?php

namespace App\Actions;

use App\Models\User;

class UserTokenAction
{
    public function createUserToken(User $user): string
    {
        return $user->createToken(User::TOKEN_NAME)->accessToken;
    }
}
