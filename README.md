# Restaurant Api

This is a restaurant rest api with Laravel backend and Angular frontend. The project is dockerized.

API documentation is created in the `openapi.yaml` file, in the root of the project. 

## Installation

- git clone https://gitlab.com/tgergo1997/restaurant-api.git
- duplicate .env.example the project root directory and save it as .env, and set MYSQL_PASSWORD value
- duplicate .env.example in the laravel-module/ directory and save it as .env, and set DB_PASSWORD value
- The 2 value must be the same

## Run project

- docker-compose up -d --build
- docker exec -it php sh
- composer install
- php artisan key:generate
- php artisan migrate
- php artisan passport:install

The project is accessible at: `localhost:4200`.