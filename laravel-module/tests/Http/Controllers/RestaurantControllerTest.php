<?php

namespace Tests\Http\Controllers;

use App\Models\Restaurant;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class RestaurantControllerTest extends TestCase
{
    use DatabaseTransactions;

    private const URL = 'http://localhost:4200/api/restaurants';

    public function test_store(): void
    {
        $user = User::first();

        $restaurant = Restaurant::factory()->definition();

        $this->actingAs($user, 'api')
            ->post(self::URL, $restaurant)
            ->assertSuccessful();

        $this->assertDatabaseHas('restaurants', [
            'user_id' => $user->id,
            'name' => $restaurant['name'],
            'description' => $restaurant['description'],
            'opening_time' => $restaurant['opening_time'],
            'closing_time' => $restaurant['closing_time'],
        ]);
    }
}
