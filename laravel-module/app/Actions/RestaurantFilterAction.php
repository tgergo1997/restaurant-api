<?php

namespace App\Actions;

use App\Models\Restaurant;
use Illuminate\Contracts\Database\Query\Builder;
use Illuminate\Support\Collection;

class RestaurantFilterAction
{
    public function filerRestaurants(array $filter): Collection
    {
        $userId = $filter['user_id'];

        return Restaurant::query()
            ->when($userId, fn (Builder $query) => $query
                ->where('user_id', $userId))
            ->get();
    }
}
