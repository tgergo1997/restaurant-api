import {Injectable} from '@angular/core';
import {GlobalConstants} from "../common/global-constants";
import {HttpClient} from '@angular/common/http';
import {Restaurant} from "../models/Restaurant";

@Injectable({
  providedIn: 'root'
})
export class RestaurantService {

  private readonly url: string;

  constructor(private http: HttpClient) {
    this.url = GlobalConstants.apiURL + 'restaurants';
  }

  public listMyRestaurants() {
    return this.http.get(this.url + '/my-restaurants');
  }

  public create(restaurant: Restaurant) {
    return this.http.post(this.url, restaurant);
  }

  public update(restaurant: Restaurant) {
    return this.http.patch(this.url + '/' + restaurant.id, restaurant);
  }

  public delete(restaurant: Restaurant) {
    return this.http.delete(this.url + '/' + restaurant.id);
  }
}
